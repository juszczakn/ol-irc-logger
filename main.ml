open Core
open Lwt
module IrcClient = Irc_client_lwt

module Irc_logging = struct

  type irc_details = {
    host: string;
    port: int;
    nick: string;
  }

  let cleanup_nick nick =
    let rgx = Re2.create_exn "[^!]+" in
    let rgx_found = Re2.find_first rgx nick in
    match rgx_found with
    |Ok s -> s
    |Error _ -> nick

  let irc_connect { host; port; nick; } =
    let realname = "Demo IRC bot" in
    let username = nick in
    Lwt_unix.gethostbyname host
    >>= fun he -> IrcClient.connect ~addr:(he.Lwt_unix.h_addr_list.(0))
      ~port ~username ~mode:0 ~realname ~nick ()

  let irc_join connection channel =
    (* let _ = IrcClient.send ~connection (Irc_message.names ~chans:[channel]) in *)
    let _ = IrcClient.send_join ~connection ~channel in
    Lwt.return connection

  let priv_join_msg connection msg =
    let rgx = Re2.create_exn "\\s*join (#[A-Za-z]+)$" in
    let rgx_found = Re2.first_match rgx msg in
    match rgx_found with
    |Ok m -> (match Re2.Match.get ~sub:(`Index 1) m with 
        |Some channel -> let _ = irc_join connection channel in Some ("joined " ^ channel)
        |None -> None)
    |Error _ -> None

  let match_priv_cmd details connection target msg =
    if details.nick <> target then
      None
    else
      priv_join_msg connection msg

  let irc_callback details connection result =
    let open Irc_message in
    match result with
    | Result.Ok msg ->
      (
        let sender = match msg.prefix with |Some a -> a |None -> "" in
        let nick = cleanup_nick sender in
        (match msg.command with
         (* |NAMES l -> Lwt_io.printf "%s\n" (String.concat l) *)
         |PRIVMSG (target, msg) -> (
             match match_priv_cmd details connection target msg with
             |Some log_msg -> Lwt_io.printf "%s\n" ("matched cmd. msg: " ^ log_msg)
             |None -> 
               Db.insert_msg msg nick target; (* Lwt.return_unit *)
               Lwt_io.printf "%s\n" (target ^ "|" ^ nick ^ ": " ^ msg)
           )
         |_ ->
           (* Lwt_io.printf "%s\n" (to_string msg) *)
           Lwt.return_unit
        )
      )
    | Result.Error e ->
      Lwt_io.printl e


  let lwt_main lwt_connection details channel =
    let callback = irc_callback details in
    lwt_connection
    >>= (fun c -> irc_join c channel)
    >>= (fun connection ->
        IrcClient.listen ~connection ~callback ()
        >>= fun () -> IrcClient.send_quit ~connection
      )
end

let () =
  let open Command.Let_syntax in
  Command.basic
    ~summary:""
    [%map_open
      let host =
        flag "host" (required string) ~doc:"HOST host to connect to"
      and port =
        flag "port" (required int) ~doc:"PORT for HOST"
      and nick =
        flag "nick" (required string) ~doc:"NICK for bot"
      and chan =
        flag "chan" (required string) ~doc:"CHAN to join"
      in
      fun () ->
        let details: Irc_logging.irc_details = { host; port; nick; } in
        let connection = Irc_logging.irc_connect details in
        Lwt_main.run (Irc_logging.lwt_main connection details chan)
    ]
  |> Command.run
