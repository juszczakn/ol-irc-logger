open Core
open Sqlite3
open Timeutil

(* TODO why hardcode this yo? *)
let db = db_open "./test.db"

let create_sqls = [
  "
create table if not exists channel
(
  channel_id integer primary key
  ,name varchar(255) not null
  ,created_time integer not null

  ,constraint unq_channel_name unique (name)
)
";
  "
create table if not exists message
(
  message_id integer primary key
  ,sent_time integer not null
  ,from_nick integer not null
  ,channel_id integer not null
  ,message text not null
  ,created_time integer not null

  ,foreign key (channel_id) references channel (channel_id)
)
"
]

let () = List.iter create_sqls ~f:(fun s -> let _ = exec db s in ())

type t_queries = {
  insert_msg: string;
  merge_channel: string;
  get_messages: string;
}

let queries = {
  get_messages="
select
  m.from_nick,
  c.name,
  m.message,
  m.sent_time
from
  message m
join
  channel c on m.channel_id = c.channel_id
where
  m.sent_time between :start_time and :end_time
order by
  sent_time desc
";

  insert_msg="
insert into message
(
  message
  ,from_nick
  ,channel_id
  ,sent_time
  ,created_time
)
values
(
  :message
  ,:from_nick
  ,(select channel_id from channel where name = :name)
  ,:sent_time
  ,:created_time
)
";

  merge_channel="
insert or ignore into channel
(
  name,
  created_time
)
values
(
  :name
  ,:created_time
)
";
}

(* utility for binding params for statements *)
let bind_tup s tup =
  let idx, param = tup in
  let _ = bind s idx param in
  ()

let bind_tups s bindings =
  List.iter bindings ~f:(bind_tup s)

let merge_channel ?(created_time=(curr_time ())) name =
  let name, created_time = (Data.TEXT name, Data.INT created_time) in
  let statement = prepare db queries.merge_channel in
  let _ = bind statement 1 name in
  let _ = bind statement 2 created_time in
  let _ = step statement in
  ()


let insert_msg ?(created_time=(curr_time ())) ?(sent_time=(curr_time ())) msg from_nick channel_name =
  (* create channel if not already *)
  let () = merge_channel channel_name ~created_time in
  (* stupid typecasting *)
  let created_time, msg, from_nick, channel_name, sent_time =
    (Data.INT created_time, Data.TEXT msg,
     Data.TEXT from_nick, Data.TEXT channel_name,
     Data.INT sent_time) in
  (* insert msg *)
  let s = prepare db queries.insert_msg in
  let bindings = [
    1, msg;
    2, from_nick;
    3, channel_name;
    4, sent_time;
    5, created_time;
  ] in
  let bind_tup = bind_tup s in
  let () = List.iter bindings ~f:bind_tup in
  let _ = step s in
  ()


type t_message = {
  channel: string;
  from_nick: string;
  message: string;
  sent_time: int64;
}

let is_done rc = match rc with
  |Rc.ROW -> false
  (* |Rc.DONE -> true
   * |Rc.EMPTY -> true *)
  |_ -> true

let next_row stmt = step stmt

let v_to_int v = match v with |Data.INT i -> i |_ -> 0L
let v_to_str v = Data.to_string v
let get_int a i = v_to_int (Array.nget a i)
let get_str a i = v_to_str (Array.nget a i)

let map_rows stmt f =
  let data = ref [] in
  let _ = while not (is_done (next_row stmt)) do
      let rowdata = row_data stmt in
      let rowheaders = row_names stmt in
      let a = f rowdata rowheaders in
      data := a :: !data
    done in
  !data

let get_messages ?(start_time=(get_past_time 1)) ?(end_time=(curr_time ())) () =
  let prep_stmt = prepare db queries.get_messages in
  let () = bind_tups prep_stmt [
      1, Data.INT start_time;
      2, Data.INT end_time;
    ] in
  map_rows prep_stmt (fun rowdata _ ->
      {
        from_nick=(get_str rowdata 0);
        channel=(get_str rowdata 1);
        message=(get_str rowdata 2);
        sent_time=(get_int rowdata 3);
      }
    )
