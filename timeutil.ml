open Core

let curr_time () = Int64.of_int (int_of_float (Unix.time ()))

let get_past_time hours_past =
  let now = Int.of_int64_exn (curr_time ()) in
  let ms_past = 1000 * 60 * hours_past in
  Int64.of_int (now - ms_past)
