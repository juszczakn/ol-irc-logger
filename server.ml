open Core
open Opium.Std

(* type person = {
 *   name: string;
 *   age: int;
 * }
 *
 * let json_of_person { name ; age } =
 *   let open Ezjsonm in
 *   dict [ "name", (string name)
 *        ; "age", (int age) ]
 *
 * let print_param = put "/hello/:name" begin fun req ->
 *     `String ("Hello " ^ param req "name") |> respond'
 *   end
 *
 * let print_person = get "/person/:name/:age" begin fun req ->
 *     let person = {
 *       name = param req "name";
 *       age = "age" |> param req |> int_of_string;
 *     } in
 *     `Json (person |> json_of_person) |> respond'
 *   end *)

let json_of_messages messages =
  let open Ezjsonm in
  let open Db in
  list (fun m ->
      dict [
        "from_nick", (string m.from_nick);
        "channel", (string m.channel);
        "message", (string m.message);
        "sent_time", (int64 m.sent_time);
      ]
    ) messages

let print_messages = get "/messages" begin fun _req ->
    let messages = Db.get_messages ~start_time:1000L () in
    `Json (messages |> json_of_messages) |> respond'
  end

let _ =
  App.empty
  (* |> print_param
   * |> print_person *)
  |> print_messages
  |> App.run_command
